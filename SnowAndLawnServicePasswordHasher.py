#File Name: SnowAndLawnPasswordHasher.py
#Authors: Nick Horn and Thad Bennett
#Last Updated: 4-23-18
#Purpose: Sets up database login information for use in SnowAndLawnServiceApp.py

#Special Notes:
#This program has to be run at least once locally to create the login database and set up a user.
#This program is ment for an Administrator to use to set up log ins for the secretaries of the company
#that use the scheduler app.
#Uses passlib.hash to create salted hashes from passwords to be stored in the database.

from passlib.hash import pbkdf2_sha256
from pymongo import MongoClient

client = MongoClient()
db = client.SnowAndLawnServiceLogin
login = db.login

try:
    userName = input('Enter UserName to add to database: ')
    password = input('Enter Password to be hashed: ')
    
    hash = pbkdf2_sha256.encrypt(password, rounds=200000, salt_size=16)
    
    newUser = {'userName': userName, 'hash': hash}
    login.insert_one(newUser)
    
    print('user ' + userName + ' added to appLogin db')
except Exception as e:
    print('\nInsert failed: ', e)
    
