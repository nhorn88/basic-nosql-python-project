# Basic NoSQL Python Project

This was a school group project that exposed us to python and MongoDB for the first time.  Performs basic CRUD operations on a database and includes SHA256 password encryption to access those operations.