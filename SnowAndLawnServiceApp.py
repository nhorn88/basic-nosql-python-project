#File Name: SnowAndLawnServiceApp.py
#Authors: Nick Horn and Thad Bennett
#Last Updated: 4-23-18
#Purpose: Main driver for Snow and Lawn Service scheduling program.  Performs basic CRUD operations on a Mongo database.

#Special Notes:
#SnowAndLawnServicePasswordHasher.py should be run at least once first in order to set up login info for accessing the database.
#Uses SnowAndLawnServiceTools.py to access the database.

from passlib.hash import pbkdf2_sha256
from pymongo import MongoClient
from SnowAndLawnServiceTools import SnowAndLawnServiceTools

loginClient = MongoClient()
dbLogin = loginClient.SnowAndLawnServiceLogin
login = dbLogin.login

try:
    userName = input('Enter User Name: ')
    password = input('Enter Password: ')
    
    result = login.find_one({'userName':userName})
    if(result): #check if result is not empty
        hash = result['hash']
        loggedIn = pbkdf2_sha256.verify(password, hash)
    else:
        raise Exception('That user does not exist.')
    
except Exception as e:
    print(str(e))
    loggedIn = False
    
    
loginClient.close()

if(loggedIn):
    print('Log in successful!\n')
    
    tools = SnowAndLawnServiceTools()
    
    while True:
        try:
            userChoice = int(input('Enter 0 to exit program\n1 to create work orders\n2 to update work orders\n3 to cancel work orders\n4 to search work orders\nEnter: '))

            if userChoice == 0:
                print ('Now exiting program')
                break # Exit program
            elif userChoice == 1: #Insert
                #generate Work order
                while True:
                    try:
                        yearInput = int(input('\nEnter date info Year (yyyy): '))
                        if yearInput < 2017 or yearInput > 2050:
                            print('\nInvalid year, please try again. Range[2017-2050]\n')
                            continue
                        monthInput = int(input('Enter date info Month (mm): '))
                        if monthInput < 1 or monthInput > 12:
                            print('\nInvalid month, please try again. Range[1-12]\n')
                            continue
                        dayInput = int(input('Enter date info Day (dd): '))
                        if dayInput < 1 or dayInput > 31:
                            print('\nInvalid day, please try again. Range[1-31]\n')
                            continue
                        hoursInput = int(input('\nEnter date info Hour in military time: '))
                        if hoursInput < 0 or hoursInput > 24:
                            print('\nInvalid hour, please try again. Range[0-24]\n')
                            continue
                        minuteInput = int(input('Enter date info Minutes: '))
                        if minuteInput < 0 or minuteInput > 59:
                            print('\nInvalid minute, please try again. Range[0-59]\n')
                            continue
                    
                        customerName = input('\nEnter Customer full name: ')
                        phoneNum = input('Enter phone number: ')
                        customerAddress = input('Enter address: ')
                    
                        price = float(input('\nEnter price quote: '))
                        if price <= 0:
                            print('\nInvalid price quote, entry must be non-negative. Please try again.\n')
                            continue
                    
                        jobType = ''
                        typeChoice = int(input('Enter 1 for snow removal, 2 lawn care: '))
                        if typeChoice == 1:
                            jobType = 'snow'
                            
                        elif typeChoice == 2:
                            jobType = 'lawn'
                            
                        else:
                            print('\nInvalid data entered! Please try again.\n')
                            continue
                            
                        instruction = input('Enter special instructions: ')
                        break
                    except Exception as e:
                        print('\nInvalid data entered! Please try again.\n')
                                        
                tools.createWorkOrder(yearInput, monthInput, dayInput, hoursInput, minuteInput, customerName, phoneNum, customerAddress, price, jobType, instruction)  	 		
          
            elif userChoice == 2: # Update
                while True:
                    try:
                        updateChoice = int(input('\nEnter 0 to return to previous menu\n1 to update date\n2 to update time\n3 to update status to complete\n4 to update status to not-complete\n5 to update price quoted\n6 to update special instrucctions\nEnter: '))
                        
                        if updateChoice == 0:
                            print ('\nReturning to previous menu.\n')
                            break # Exit program
                        elif updateChoice == 1: #date
                            while True:
                                try:
                                    workOrder = input('\nEnter work order id to be updated: ')
                                    yearInput = int(input('\nEnter date info Year (yyyy): '))
                                    if yearInput < 2017 or yearInput > 2050:
                                        print('\nInvalid year, please try again. Range[2017-2050]\n')
                                        continue
                                    monthInput = int(input('Enter date info Month (mm): '))
                                    if monthInput < 1 or monthInput > 12:
                                        print('\nInvalid month, please try again. Range[1-12]\n')
                                        continue
                                    dayInput = int(input('Enter date info Day (dd): '))
                                    if dayInput < 1 or dayInput > 31:
                                        print('\nInvalid day, please try again. Range[1-31]\n')
                                        continue
                                    break
                                except Exception as e:
                                    print('\nInvalid data entered! Please try again.\n')
                                    
                            tools.updateWorkOrderDate(workOrder, yearInput, monthInput, dayInput)
                            
                        elif updateChoice == 2: #time
                            while True:
                                try:
                                    workOrder = input('\nEnter work order id to be updated: ')
                                    hoursInput = int(input('\nEnter date info Hour in military time: '))
                                    if hoursInput < 0 or hoursInput > 24:
                                        print('\nInvalid hour, please try again. Range[0-24]\n')
                                        continue
                                    minuteInput = int(input('Enter date info Minutes: '))
                                    if minuteInput < 0 or minuteInput > 59:
                                        print('\nInvalid minute, please try again. Range[0-59]\n')
                                        continue
                                    break
                                except Exception as e:
                                    print('\nInvalid data entered! Please try again.\n')
                                    
                            tools.updateWorkOrderTime(workOrder, hoursInput, minuteInput)
                            
                        elif updateChoice == 3: #status complete
                            workOrder = input('\nEnter work order id to mark complete: ')
                            statusInput = 'complete'
                            tools.updateWorkOrderStatus(workOrder, statusInput)
                            
                        elif updateChoice == 4: #status not-complete
                            workOrder = input('\nEnter work order id to mark not-complete: ')
                            statusInput = 'not-complete'
                            tools.updateWorkOrderStatus(workOrder, statusInput)
                            
                        elif updateChoice == 5: #price
                            while True:
                                try:
                                    workOrder = input('\nEnter work order id to update quoted price: ')
                                    updatePrice = float(input('\nEnter new quoted price: '))
                                    if updatePrice <= 0:
                                        print('\nInvalid price quote, entry must be non-negative. Please try again.\n')
                                        continue
                                    break
                                
                                except Exception as e:
                                    print('\nInvalid data entered! Please try again.\n')
                            
                            tools.updateWorkOrderPriceQuoted(workOrder, updatePrice)
                        
                        elif updateChoice == 6: #special instructions
                            workOrder = input('\nEnter work order id to update special instructions: ')
                            updateInstruction = input('\nEnter special instructions: ')
                            tools.updateWorkOrderSpecialInstructions(workOrder, updateInstruction)
                             
                        else:
                            print('\nInvalid data entered!  Please try again.\n')
                            
                    except Exception as e:
                        print('\nInvalid data entered! Please try again.\n')
                            
            elif userChoice == 3: # Delete
                workOrderInput = input('\nEnter work order id to be cancelled/deleted: ')
                areYouSure = input('Are you sure you want to cancel work order ' + workOrderInput + ' ?\n'
                                   + 'Cancelled orders can not be recovered.\n Enter "y" to proceed, any other character aborts command: ')
                if areYouSure == 'y':
                    tools.cancelWorkOrder(workOrderInput)
                else:
                    print('Cancel aborted.\n')
                
            elif userChoice == 4: # Search
                while True:
                    try:
                        searchChoice = int(input('\nEnter 0 to go back to main menu\n1 to search all\n2 to search by work order ID\n3 to search by status'
                                             + '\n4 to search by job type\n5 to search by customer name\nEnter: '))
                        if searchChoice == 0:
                            print('Returning to previous menu.\n')
                            break
                        
                        elif searchChoice == 1: #search all
                            tools.searchWorkOrders('all')
                            
                        elif searchChoice == 2: #search work order id
                            searchInput = input('\nEnter work order id to be searched: ')
                            tools.searchWorkOrders('workOrderId', searchInput)
                            
                        elif searchChoice == 3: #search status
                            while True:
                                try:
                                    statusChoice = int(input('\nEnter 1 to search by not-complete\n2 to search by complete\nEnter: '))
                                    if statusChoice == 1:
                                        searchInput = 'not-complete'
                                        
                                    elif statusChoice == 2:
                                        searchInput = 'complete'
                                        
                                    else:
                                        print('\nInvalid data entered!  Please try again.\n')
                                        continue
                                    break
                                except Exception as e:
                                    print('\nInvalid data entered! Please try again.\n')
                            
                            tools.searchWorkOrders('status', searchInput)
                            
                        elif searchChoice == 4: #search job type
                            while True:
                                try:
                                    typeChoice = int(input('\nEnter 1 to search by snow removal\n2 to search by lawn care\nEnter: '))
                                    if typeChoice == 1:
                                        searchInput = 'snow'
                                        
                                    elif typeChoice == 2:
                                        searchInput = 'lawn'
                                        
                                    else:
                                        print('\nInvalid data entered!  Please try again.\n')
                                        continue
                                    break
                                except Exception as e:
                                    print('\nInvalid data entered! Please try again.\n')
                            
                            tools.searchWorkOrders('jobType', searchInput)
                            
                        elif searchChoice == 5: #search customer name
                            searchInput = input('\nEnter customer name to be searched (partial search supported): ')
                            tools.searchWorkOrders('customerName', searchInput)
                        
                        else:
                            print('\nInvalid data entered! Please try again.\n')
                            
                    except Exception as e:
                        print('\nInvalid data entered! Please try again.\n')
                
            else:
                print ('\nInvalid data entered! Please try again.\n')
                
        except Exception as e:
            print ('\nInvalid data entered! Please try again.\n')
            
    #end main while loop
    tools.closeConnection()
else: #not logged in, end of program
    print('Log in failed!')
    