#File Name: SnowAndLawnServiceTools.py
#Authors: Nick Horn and Thad Bennett
#Last Updated: 4-23-18
#Purpose: Provides functions needed to perform basic CRUD operations on the Mongo database.

#Special Notes:
#Used by SnowAndLawnServiceApp.py

from pymongo import MongoClient
import pymongo
import random  #used in generating work order ids
import string  #used in generating work order ids
import datetime

class SnowAndLawnServiceTools(object):
    
    def __init__(self):
        self.client = MongoClient()
        self.db = self.client.SnowAndLawnService
        self.app = self.db.workOrders

    def closeConnection(self):
        self.client.close()
 
    #returns unique work order id
    def generateWorkOrderId(self):
        workOrder = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(5))
        
        #check if generated id exists in db, (very unlikely, but just in case)
        while(True):
            result = self.app.find({'workOrderId':workOrder})
            if(result.count() == 0): #if result is empty
                return workOrder
            else:
                workOrder = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))
                

    def createWorkOrder(self, year, month, day, hours, minutes, customerName, phone, address, priceQuoted, jobType, instructions):
        workOrder = self.generateWorkOrderId()
        serviceDate = datetime.datetime(year, month, day, hours, minutes, 0, 0)
        self.app.insert_one({'workOrderId':workOrder, 'dateTime':serviceDate, 'customer':{'name':customerName, 'phone':phone, 'address':address}, 'priceQuoted':priceQuoted, 'status':'not-complete', 'jobType':jobType, 'specialInstructions':instructions})
        print('\nWork order ' + workOrder + ' created for ' + customerName + '\n')
        
    def searchWorkOrders(self, searchType, searchInput=''):
        if searchType == 'all':
            result = self.app.find()
        elif searchType == 'customerName':
            #partial search and case insensitivity supported
            result = self.app.find({'customer.name': {'$regex': searchInput, '$options': 'i'}})
        else:
            result = self.app.find({searchType : searchInput})
            
        result.sort('dateTime')
        print('\n-----------------------------------\nSearch Results for ' + searchType + ': ' + searchInput)
        for w in result:
            dt = w['dateTime']
            d = dt.date().isoformat()
            t = dt.time()
            customer = w['customer']
            print('\n----  Work Order: ' + w['workOrderId'] + '\nDate: ' + str(d)
                  + '\nTime: ' + str(t)+ '\n\nCustomer: ' + customer['name'] + '\nPhone Number: '
                  + customer['phone'] + '\nAddress: ' + customer['address'] + '\n\nPrice Quoted: $'
                  + str(w['priceQuoted']) + '\nJob Type: ' + w['jobType'] + '\nStatus: ' + w['status']
                  + '\n\nSpecial Instructions: ' + w['specialInstructions'] + '\n----')
        print('\n-----------------------------------\n')
        
    def updateWorkOrderDate(self, workOrder, newYear, newMonth, newDay):
        result = self.app.find_one({'workOrderId': workOrder})
        if(result):
            dt = result['dateTime']
            dt = dt.replace(year = newYear, month = newMonth, day = newDay)
            self.app.update_one({'workOrderId': workOrder}, {'$set':{'dateTime': dt}})
            print('\nWork Order ' + workOrder + ' updated successfully.\n')
        else:
            print('\nNo work order found for id: ' + workOrder + ' please try again.\n')
            
    def updateWorkOrderTime(self, workOrder, newHour, newMinute):
        result = self.app.find_one({'workOrderId': workOrder})
        if(result):
            dt = result['dateTime']
            dt = dt.replace(hour = newHour, minute = newMinute)
            self.app.update_one({'workOrderId': workOrder}, {'$set':{'dateTime': dt}})
            print('\nWork Order ' + workOrder + ' updated successfully.\n')
        else:
            print('\nNo work order found for id: ' + workOrder + ' please try again.\n')
            
    def updateWorkOrderStatus(self, workOrder, status):
        result = self.app.find_one({'workOrderId': workOrder})
        if(result):
            self.app.update_one({'workOrderId': workOrder}, {'$set':{'status': status}})
            print('\nWork Order ' + workOrder + ' updated successfully.\n')
        else:
            print('\nNo work order found for id: ' + workOrder + ' please try again.\n')
            
    def updateWorkOrderPriceQuoted(self, workOrder, price):
        result = self.app.find_one({'workOrderId': workOrder})
        if(result):
            self.app.update_one({'workOrderId': workOrder}, {'$set':{'priceQuoted': price}})
            print('\nWork Order ' + workOrder + ' updated successfully.\n')
        else:
            print('\nNo work order found for id: ' + workOrder + ' please try again.\n')
            
    def updateWorkOrderSpecialInstructions(self, workOrder, specialInstructions):
        result = self.app.find_one({'workOrderId': workOrder})
        if(result):
            self.app.update_one({'workOrderId': workOrder}, {'$set':{'specialInstructions': specialInstructions}})
        else:
            print('\nNo work order found for id: ' + workOrder + ' please try again.\n')
            
    def cancelWorkOrder(self, workOrder):
        result = self.app.find_one({'workOrderId': workOrder})
        if(result):
            status = result['status']
            
            if status == 'not-complete':
                self.app.delete_one({'workOrderId': workOrder})
                print('\nWork Order ' + workOrder + ' cancelled and removed from system.\n')
            elif status == 'complete':
                print('\nWork order ' + workOrder + ' is marked complete and thus cannot be cancelled.\n')
            else:
                print('\nWork order ' + workOrder + ' has a status error, please refer this work order to an admin for review.')
        else:
            print('\nNo work order found for id: ' + workOrder + ' please try again.\n')
